import itertools, collections
from .curry import curry


__all__ = ['first', 'second', 'last', 'take', 'drop', 'nth', 'cons']


def first(seq):
    """Returns the first element of `seq`.
    """
    return next(iter(seq))


def second(seq):
    """Returns the second element of `seq`.
    """
    seq = iter(seq)
    next(seq)
    return next(seq)


def last(seq):
    """Returns the last element of `seq`.
    """
    return tuple(collections.deque(seq, 1))[0]


@curry
def take(n, seq):
    """Returns the first `n` elements of `seq`.
    This function is "lazy", meaning that the output
    is not instantiated, but in iterator form.
    """
    return itertools.islice(seq, n)


@curry
def drop(n, seq):
    """Drops the first `n` elements of `seq`.
    This function is "lazy", meaning that the output
    is not instantiated, but in iterator form.
    """
    return itertools.islice(seq, n, None)


@curry
def nth(n, seq):
    """Returns the `n`th element of `seq`.
    """
    if isinstance(seq, (tuple, list)):
        return seq[n]
    else:
        return next(itertools.islice(seq, n, None))


@curry
def cons(e, seq):
    """Prepends `e` to `seq`.
    """
    return itertools.chain([e], seq)
