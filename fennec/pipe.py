from functools import partial


__all__ = ['pipe', 'piped']


class Infix(object):
    def __init__(self, func):
        self.func = func
    def __or__(self, other):
        return self.func(other)
    def __ror__(self, other):
        return Infix(partial(self.func, other))
    def __call__(self, v1, v2):
        return self.func(v1, v2)


@Infix
def pipe(x, f):
    return f(x)


def piped(x, *funcs):
    for f in funcs:
        x = f(x)
    return x
