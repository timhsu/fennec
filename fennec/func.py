from functools import reduce


__all__ = ['comp', 'chain', 'juxt']


def comp2(f, g):
    """Composes two functions.
    Concept:
        `comp2(f, g) = f(g(•))`
    """
    def h(*args, **kwargs):
        return f(g(*args, **kwargs))
    return h


def comp(*funcs):
    """Composes multiple functions.
    Concept:
        `comp(f, g, h) = f(g(h(•)))`
    """
    return reduce(comp2, funcs, lambda x: x)


def chain(*funcs):
    """Composes multiple functions in the reverse order.
    Concept:
        `chain(f, g, h) = h(g(f(•)))`
    """
    return reduce(comp2, reversed(funcs), lambda x: x)


def juxt(*funcs):
    """Juxtaposes multiple functions.
    Concept:
        `juxt(f, g, h) = [f(•), g(•), h(•)]`
    """
    def h(*args, **kwargs):
        return tuple(f(*args, **kwargs) for f in funcs)
    return h
