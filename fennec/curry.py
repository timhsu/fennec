from functools import wraps, partial
from inspect import signature


__all__ = ['curry']


def curry(func):
    @wraps(func)
    def curried(*args, **kwargs):
        if len(args) + len(kwargs) >= len(signature(func).parameters):
            return func(*args, **kwargs)

        @wraps(func)
        def new_curried(*args2, **kwargs2):
            return curried(*(args + args2), **dict(kwargs, **kwargs2))

        return new_curried

    return curried
