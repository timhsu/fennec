# fennec

A very lightweight library of functions and tools for enabling writing Python in a more functional programming (FP) style.

Disclaimer
: Many implementations written here are adopted from elsewhere. I do not claim credits or ownership for the codes written in this repository. See [References](#references) for more details.


## Usage

This small library mainly features three functionalities: function composition, curried functions, and pipe syntax. Some usage examples are shown below.


### Function composition

Use `comp` or `chain` to compose multiple functions together.
The example below can actually be more succinctly done with the `pathlib` library. But this example demonstrates that you can chain multiple functions together in one line concisely and conveniently.

```python
from fennec import *
from os.path import basename, splitext

fname = '/path/to/file/123.txt'
fname_to_idx = chain(basename, splitext, first, int)
fname_to_idx(fname)
# >>> 123
```

### Curried functions

You can apply `curry` in function form for functions defined elsewhere, or you can apply `@curry` in decorator form for your own function definitions.

```python
from fennec import *
from operator import add, mul

add, mul = curry(add), curry(mul)
f = chain(add(10), mul(3))
f(5)
# >>> 45

@curry
def my_sum(a,b,c,d,e):
    return a+b+c+d+e

my_sum(1,2)(3)(4,5)   # >>> 15
my_sum(1)(2,3)(4,5)   # >>> 15
my_sum(1)(2)(3)(4,5)  # >>> 15
my_sum(1,2)(3,4,5)    # >>> 15
my_sum(1,2,3,4,5)     # >>> 15
```

### Pipe operator 

Use the `|pipe|` operator in a similar way that you would use the pipe ('|') symbol in Bash. But the `|pipe|` operator is not that simple to type. So alternatively, you can use `piped` to pipe from a variable to a sequence of functions.

```python
from fennec import *
from operator import add

add = curry(add)
foo = 1 |pipe| add(2) \
        |pipe| add(3) \
        |pipe| add(4) 

foo |pipe| print
# >>> 10

bar = piped(1, add(2), add(3), add(4))
print(bar)
# >>> 10
```


## Why

I wrote these tools primarily to learn FP and to have simple functions handy when I need to write Python code in a more readable, concise, convenient manner. It is meant to be a small, simple library (a couple hundreds of lines of code or less), not a sizeable, extensive project.

Also, I really want to write in Julia for my work but I can't due to the lack of ecosystem libraries. So I'm stuck with Python for now...


## References

There are other much better packages or repositories to use if you wish to implement your own FP code in Python. In fact, a lot of the codes here are adopted from some of the following resources:
* [fn.py][1]
* [pytooz][2]
* [Functional pipe syntax in Python][3]
    * [pipe][4]
    * [pipes][5]
* [Coconut Lang][6] (not exactly Python)

[1]: https://github.com/kachayev/fn.py
[2]: https://github.com/pytoolz/toolz
[3]: https://stackoverflow.com/questions/28252585/functional-pipes-in-python-like-from-rs-magrittr
[4]: https://github.com/JulienPalard/Pipe
[5]: https://github.com/robinhilliard/pipes
[6]: http://coconut-lang.org/


## Notes

Avoid the use of keyword arguments. The FP mechanisms such as curried functions (with the `@curry` decorator) and the `|pipe|` infix operator may not be universally applicable to functions with keyword arguments.
